use druid::Code;

pub enum KeyboardLayoutItem {
    Key(f64, bool, &'static str, Code),  // Width, Sticky, Label, Key Code
    Space(f64),                          // Width
}

pub type KeyboardLayout = Vec<Vec<KeyboardLayoutItem>>;

lazy_static! {
    pub static ref LAYOUT_QWERTY: KeyboardLayout = vec![
        vec![
            KeyboardLayoutItem::Key(1.0,  false, "Esc",       Code::Escape),
            KeyboardLayoutItem::Space(0.5),
            KeyboardLayoutItem::Key(1.0,  false, "F1",        Code::F1),
            KeyboardLayoutItem::Key(1.0,  false, "F2",        Code::F2),
            KeyboardLayoutItem::Key(1.0,  false, "F3",        Code::F3),
            KeyboardLayoutItem::Key(1.0,  false, "F4",        Code::F4),
            KeyboardLayoutItem::Key(1.0,  false, "F5",        Code::F5),
            KeyboardLayoutItem::Key(1.0,  false, "F6",        Code::F6),
            KeyboardLayoutItem::Key(1.0,  false, "F7",        Code::F7),
            KeyboardLayoutItem::Key(1.0,  false, "F8",        Code::F8),
            KeyboardLayoutItem::Key(1.0,  false, "F9",        Code::F9),
            KeyboardLayoutItem::Key(1.0,  false, "F10",       Code::F10),
            KeyboardLayoutItem::Key(1.0,  false, "F11",       Code::F11),
            KeyboardLayoutItem::Key(1.0,  false, "F12",       Code::F12),
            KeyboardLayoutItem::Space(0.5),
            KeyboardLayoutItem::Key(1.0,  false, "Del",       Code::Delete),
        ],            
        vec![
            KeyboardLayoutItem::Key(1.5,  false, "`",         Code::Backquote),
            KeyboardLayoutItem::Key(1.0,  false, "1",         Code::Digit1),
            KeyboardLayoutItem::Key(1.0,  false, "2",         Code::Digit2),
            KeyboardLayoutItem::Key(1.0,  false, "3",         Code::Digit3),
            KeyboardLayoutItem::Key(1.0,  false, "4",         Code::Digit4),
            KeyboardLayoutItem::Key(1.0,  false, "5",         Code::Digit5),
            KeyboardLayoutItem::Key(1.0,  false, "6",         Code::Digit6),
            KeyboardLayoutItem::Key(1.0,  false, "7",         Code::Digit7),
            KeyboardLayoutItem::Key(1.0,  false, "8",         Code::Digit8),
            KeyboardLayoutItem::Key(1.0,  false, "9",         Code::Digit9),
            KeyboardLayoutItem::Key(1.0,  false, "0",         Code::Digit0),
            KeyboardLayoutItem::Key(1.0,  false, "-",         Code::Minus),
            KeyboardLayoutItem::Key(1.0,  false, "=",         Code::Equal),
            KeyboardLayoutItem::Key(1.5,  false, "Back",      Code::Backspace),
        ],
        vec![
            KeyboardLayoutItem::Key(1.5,  false, "Tab",       Code::Tab),
            KeyboardLayoutItem::Key(1.0,  false, "Q",         Code::KeyQ),
            KeyboardLayoutItem::Key(1.0,  false, "W",         Code::KeyW),
            KeyboardLayoutItem::Key(1.0,  false, "E",         Code::KeyE),
            KeyboardLayoutItem::Key(1.0,  false, "R",         Code::KeyR),
            KeyboardLayoutItem::Key(1.0,  false, "T",         Code::KeyT),
            KeyboardLayoutItem::Key(1.0,  false, "Y",         Code::KeyY),
            KeyboardLayoutItem::Key(1.0,  false, "U",         Code::KeyU),
            KeyboardLayoutItem::Key(1.0,  false, "I",         Code::KeyI),
            KeyboardLayoutItem::Key(1.0,  false, "O",         Code::KeyO),
            KeyboardLayoutItem::Key(1.0,  false, "P",         Code::KeyP),
            KeyboardLayoutItem::Key(1.0,  false, "[",         Code::BracketLeft),
            KeyboardLayoutItem::Key(1.0,  false, "]",         Code::BracketRight),
            KeyboardLayoutItem::Key(1.5,  false, "\\",        Code::Backslash),
        ],
        vec![
            KeyboardLayoutItem::Key(2.0,  true , "CapsLock",  Code::CapsLock),
            KeyboardLayoutItem::Key(1.0,  false, "A",         Code::KeyA),
            KeyboardLayoutItem::Key(1.0,  false, "S",         Code::KeyS),
            KeyboardLayoutItem::Key(1.0,  false, "D",         Code::KeyD),
            KeyboardLayoutItem::Key(1.0,  false, "F",         Code::KeyF),
            KeyboardLayoutItem::Key(1.0,  false, "G",         Code::KeyG),
            KeyboardLayoutItem::Key(1.0,  false, "H",         Code::KeyH),
            KeyboardLayoutItem::Key(1.0,  false, "J",         Code::KeyJ),
            KeyboardLayoutItem::Key(1.0,  false, "K",         Code::KeyK),
            KeyboardLayoutItem::Key(1.0,  false, "L",         Code::KeyL),
            KeyboardLayoutItem::Key(1.0,  false, ";",         Code::Semicolon),
            KeyboardLayoutItem::Key(1.0,  false, "'",         Code::Quote),
            KeyboardLayoutItem::Key(2.0,  false, "Enter",     Code::Enter),
            ],
        vec![
            KeyboardLayoutItem::Key(2.5,  true,  "Shift",      Code::ShiftLeft),
            KeyboardLayoutItem::Key(1.0,  false, "Z",          Code::KeyZ),
            KeyboardLayoutItem::Key(1.0,  false, "X",          Code::KeyX),
            KeyboardLayoutItem::Key(1.0,  false, "C",          Code::KeyC),
            KeyboardLayoutItem::Key(1.0,  false, "V",          Code::KeyV),
            KeyboardLayoutItem::Key(1.0,  false, "B",          Code::KeyB),
            KeyboardLayoutItem::Key(1.0,  false, "N",          Code::KeyN),
            KeyboardLayoutItem::Key(1.0,  false, "M",          Code::KeyM),
            KeyboardLayoutItem::Key(1.0,  false, ",",          Code::Comma),
            KeyboardLayoutItem::Key(1.0,  false, ".",          Code::Period),
            KeyboardLayoutItem::Key(1.0,  false, "/",          Code::Slash),
            KeyboardLayoutItem::Key(2.5,  true,  "Shift",      Code::ShiftRight),
        ],
        vec![
            KeyboardLayoutItem::Key(1.0,  true,  "Ctrl",       Code::ControlLeft),
            KeyboardLayoutItem::Key(1.0,  true,  "Alt",        Code::AltLeft),
            KeyboardLayoutItem::Key(11.0, false, "",           Code::Space),
            KeyboardLayoutItem::Key(1.0,  true,  "Alt",        Code::AltRight),
            KeyboardLayoutItem::Key(1.0,  true,  "Ctrl",       Code::ControlRight),
        ],
    ];
}
