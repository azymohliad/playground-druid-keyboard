use std::collections::HashMap;
use druid::{
    Data,
    Rect,
    Point,
    Selector,
    Code,
    WidgetPod,
    WidgetExt,
    widget::{
        prelude::*,
        Flex,
        IdentityWrapper,
    }
};

use super::layout::{KeyboardLayout, KeyboardLayoutItem};
use super::key::KeyboardKey;

pub const CMD_PRESS_KEY: Selector<Code> = Selector::new("key_pressed");
pub const CMD_RELEASE_KEY: Selector<Code> = Selector::new("key_released");

struct KeyInfo {
    widget: WidgetId,
    pressed: bool,
}

pub struct Keyboard<T> {
    keys: HashMap<Code, KeyInfo>,
    on_press_fn: Box<dyn Fn(&mut EventCtx, Code, &mut T, &Env)>,
    on_release_fn: Box<dyn Fn(&mut EventCtx, Code, &mut T, &Env)>,
    inner:  WidgetPod<T, Flex<T>>,
}

impl<T: Data> Keyboard<T> {
    pub fn new<F1,F2>(layout: &KeyboardLayout, on_press: F1, on_release: F2) -> IdentityWrapper<Self>
    where
        F1: Fn(&mut EventCtx, Code, &mut T, &Env) + 'static,
        F2: Fn(&mut EventCtx, Code, &mut T, &Env) + 'static
    {
        let kb_wid = WidgetId::next();
        let mut keys = HashMap::new();
        let mut keyboard_flex = Flex::column();
        for row_layout in layout {
            let mut row_flex = Flex::row();
            for item in row_layout {
                match *item {
                    KeyboardLayoutItem::Key(width, sticky, label, code) => {
                        let wid = WidgetId::next();
                        let key = KeyboardKey::new(label)
                            .sticky(sticky)
                            .on_press(move |ctx, _, _| ctx.submit_command(CMD_PRESS_KEY.with(code), kb_wid))
                            .on_release(move |ctx, _, _| ctx.submit_command(CMD_RELEASE_KEY.with(code), kb_wid))
                            .with_id(wid);
                        row_flex.add_flex_child(key, width);
                        keys.insert(code, KeyInfo { widget: wid, pressed: false });
                    }
                    KeyboardLayoutItem::Space(width) => {
                        row_flex.add_flex_spacer(width);
                    }
                }
            }
            keyboard_flex.add_flex_child(row_flex, 1.0);
        }

        Self {
            keys,
            on_press_fn: Box::new(on_press),
            on_release_fn: Box::new(on_release),
            inner: WidgetPod::new(keyboard_flex),
        }.with_id(kb_wid)
    }
}

impl<T: Data> Widget<T> for Keyboard<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        match event {
            Event::WindowConnected => ctx.request_focus(),
            Event::KeyDown(key_event) if !key_event.repeat => {
                if let Some(mut key) = self.keys.get_mut(&key_event.code) {
                    (self.on_press_fn)(ctx, key_event.code, data, env);
                    key.pressed = true;
                    ctx.submit_command(super::key::CMD_ACTIVATE_KEY.with(true), key.widget)
                }
            },
            Event::KeyUp(key_event) => {
                if let Some(mut key) = self.keys.get_mut(&key_event.code) {
                    (self.on_release_fn)(ctx, key_event.code, data, env);
                    key.pressed = false;
                    ctx.submit_command(super::key::CMD_ACTIVATE_KEY.with(false), key.widget)
                }
            },
            Event::Command(cmd) => {
                if let Some(&key_code) = cmd.get(CMD_PRESS_KEY) {
                    if let Some(mut key) = self.keys.get_mut(&key_code) {
                        (self.on_press_fn)(ctx, key_code, data, env);
                        key.pressed = true;
                    }
                }
                if let Some(&key_code) = cmd.get(CMD_RELEASE_KEY) {
                    if let Some(mut key) = self.keys.get_mut(&key_code) {
                        (self.on_release_fn)(ctx, key_code, data, env);
                        key.pressed = false;
                    }
                }
            }
            _ => (),
        }
        self.inner.event(ctx, event, data, env);
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        self.inner.lifecycle(ctx, event, data, env)
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &T, data: &T, env: &Env) {
        self.inner.update(ctx, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        let size = self.inner.layout(ctx, bc, data, env);    
        self.inner.set_layout_rect(ctx, data, env, Rect::from_origin_size(Point::new(0.0, 0.0), size));
        size
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        self.inner.paint(ctx, data, env);
    }
}
