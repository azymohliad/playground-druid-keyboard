use druid::{
    Data,
    Size,
    Rect,
    Point,
    Selector,
    MouseButton,
    WidgetExt,
    WidgetPod,
    widget::{
        prelude::*,
        Label,
        LabelText,
        SizedBox,
    },
};

pub const CMD_ACTIVATE_KEY: Selector<bool> = Selector::new("activate_key");

const CORNER_RADIUS: f64 = 5.0;
const HOT_BORDER: f64 = 4.0;

pub struct KeyboardKey<T> {
    is_pressed: bool,
    is_sticky: bool,
    on_press_fn: Box<dyn Fn(&mut EventCtx, &mut T, &Env)>,
    on_release_fn: Box<dyn Fn(&mut EventCtx, &mut T, &Env)>,
    inner: WidgetPod<T, SizedBox<T>>,
}

impl<T: Data> KeyboardKey<T> {
    pub fn new(label: impl Into<LabelText<T>>) -> Self {
        let inner = Label::new(label.into()).center().expand();
        Self {
            is_pressed: false,
            is_sticky: false,
            on_press_fn: Box::new(|_, _, _| {}),
            on_release_fn: Box::new(|_, _, _| {}),
            inner: WidgetPod::new(inner)
        }
    }

    pub fn sticky(mut self, is_sticky: bool) -> Self {
        self.is_sticky = is_sticky;
        self
    }

    pub fn on_press(mut self, f: impl Fn(&mut EventCtx, &mut T, &Env) + 'static) -> Self {
        self.on_press_fn = Box::new(f);
        self
    }

    pub fn on_release(mut self, f: impl Fn(&mut EventCtx, &mut T, &Env) + 'static) -> Self {
        self.on_release_fn = Box::new(f);
        self
    }
}

impl<T: Data> Widget<T> for KeyboardKey<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        let on_press = |key: &mut Self, ctx: &mut EventCtx, data, env| {
            (key.on_press_fn)(ctx, data, env);
            key.is_pressed = true;
            ctx.request_paint();
        };

        let on_release = |key: &mut Self, ctx: &mut EventCtx, data, env| {
            (key.on_release_fn)(ctx, data, env);
            key.is_pressed = false;
            ctx.request_paint();
        };

        match event {
            Event::MouseMove(mouse) if mouse.buttons.has_left() => {
                if ctx.is_hot() && !self.is_pressed {
                    (on_press)(self, ctx, data, env);
                } else if !ctx.is_hot() && self.is_pressed && !self.is_sticky {
                    (on_release)(self, ctx, data, env);
                }
            },
            Event::MouseDown(mouse) if mouse.button == MouseButton::Left => {
                if !self.is_pressed {
                    (on_press)(self, ctx, data, env);
                } else if self.is_sticky {
                    (on_release)(self, ctx, data, env);
                }
            },
            Event::MouseUp(mouse) if mouse.button == MouseButton::Left => {
                if self.is_pressed && !self.is_sticky {
                    (on_release)(self, ctx, data, env);
                }
            },
            Event::Command(cmd) => {
                if let Some(&active) = cmd.get(CMD_ACTIVATE_KEY) {
                    self.is_pressed = active;
                    ctx.request_paint();
                }
            },
            _ => ()
        }
        self.inner.event(ctx, event, data, env);
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        match event {
            LifeCycle::HotChanged(_) => ctx.request_paint(),
            _ => ()
        }
        self.inner.lifecycle(ctx, event, data, env)
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &T, data: &T, env: &Env) {
        self.inner.update(ctx, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        let size = self.inner.layout(ctx, bc, data, env);    
        self.inner.set_layout_rect(ctx, data, env, Rect::from_origin_size(Point::new(0.0, 0.0), size));
        size
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        let bounds = ctx.size().to_rect().inset(-0.5 * HOT_BORDER).to_rounded_rect(CORNER_RADIUS);
        if ctx.is_hot() {
            ctx.stroke(bounds, &env.get(druid::theme::BACKGROUND_LIGHT), HOT_BORDER);
        }
        if self.is_pressed {
            ctx.fill(bounds, &env.get(druid::theme::BACKGROUND_LIGHT));
        } else {
            ctx.fill(bounds, &env.get(druid::theme::BACKGROUND_DARK));
        }
        self.inner.paint(ctx, data, env);
    }
}