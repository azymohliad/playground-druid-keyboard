#[macro_use]
extern crate lazy_static;

use druid::{
    Data,
    Lens,
    Widget,
    AppLauncher,
    WindowDesc,
    PlatformError
};

mod keyboard;

#[derive(Clone,Data,Lens)]
pub struct AppState {
}

fn build_ui() -> impl Widget<AppState> {
    keyboard::Keyboard::new(
        &keyboard::LAYOUT_QWERTY,
        |_, key, _, _| println!("Pressed: {:?}", key),
        |_, key, _, _| println!("Released: {:?}", key)
    )
}

fn main() -> Result<(), PlatformError> {
    let state = AppState {}; 
    AppLauncher::with_window(
        WindowDesc::new(build_ui)
            .title("Keyboard")
            .window_size((800.0, 300.0))
            .with_min_size((400.0, 150.0))
    ).launch(state)?;
    Ok(())
}
